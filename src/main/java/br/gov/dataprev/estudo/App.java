package br.gov.dataprev.estudo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * Hello world!
 *
 */
@SpringBootApplication
public class App {

    private static final Logger log = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {
        System.setProperty("server.port","8080");
        SpringApplication.run(App.class, args);

        RestTemplate restTemplate = new RestTemplate();

        Post post = restTemplate.getForObject("http://localhost:8080/posts", Post.class);
        log.info(post.toString());
    }
}
