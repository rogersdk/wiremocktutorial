package br.gov.dataprev.estudo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.client.RestTemplate;

/**
 * Created by rogerionobrega on 18/04/18.
 */
@SpringBootApplication
public class App2 {

  private static final Logger log = LoggerFactory.getLogger(App2.class);

  public static void main(String[] args) {
    System.setProperty("server.port","8888");
    SpringApplication.run(App2.class, args);

    RestTemplate restTemplate = new RestTemplate();

    Post post = restTemplate.getForObject("http://localhost:8888/posts", Post.class);
    log.info(post.toString());
  }
}
