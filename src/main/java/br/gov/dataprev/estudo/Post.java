package br.gov.dataprev.estudo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by rogerionobrega on 22/03/18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Post {
  public String getHost() {
    return host;
  }

  public void setHost(String host) {
    this.host = host;
  }

  private Integer userId;
  private Integer postId;
  private String title;
  private String body;
  private String host;

  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public Integer getPostId() {
    return postId;
  }

  public void setPostId(Integer id) {
    this.postId = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  @Override
  public String toString() {
    return "Post{" +
            "userId=" + userId +
            ", postId=" + postId +
            ", title='" + title + '\'' +
            ", body='" + body + '\'' +
            ", host='" + host + '\'' +
            '}';
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Post post = (Post) o;

    return postId != null ? postId.equals(post.postId) : post.postId == null;
  }

  @Override
  public int hashCode() {
    return postId != null ? postId.hashCode() : 0;
  }
}
