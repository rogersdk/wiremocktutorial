package br.gov.dataprev.estudo;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rogerionobrega on 22/03/18.
 */
@RestController
public class PostRestController {

  private List<Post> posts = new ArrayList<>();

  public PostRestController() {
    Post post = new Post();
    post.setBody("Body Birlder");
    post.setPostId(1);
    post.setTitle("Birl title");
    posts.add(post);
  }


  @RequestMapping(value = "/posts/{postId}", method = RequestMethod.GET)
  public ResponseEntity<Post> getPost(@PathVariable String postId) {
    String server = "8888".equals(System.getProperty("server.port")) ? "App 2" : "App 1";

    Post post = new Post();
    post.setBody("Body Birlder");
    post.setPostId(Integer.parseInt(postId));
    post.setTitle("Birl title. \n server.port=" + System.getProperty("server.port"));

    post.setHost(server);

    return new ResponseEntity<Post>(post, HttpStatus.OK);
  }

  @RequestMapping(value = "/", method = RequestMethod.GET)
  public ResponseEntity<String> hello() {
    String server = "8888".equals(System.getProperty("server.port")) ? "App 2" : "App 1";


    return new ResponseEntity<String>("Hello World! Server = " + server , HttpStatus.OK);
  }

}
