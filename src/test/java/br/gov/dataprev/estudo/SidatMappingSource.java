package br.gov.dataprev.estudo;

import com.github.tomakehurst.wiremock.common.AbstractFileSource;
import com.github.tomakehurst.wiremock.common.FileSource;
import com.github.tomakehurst.wiremock.common.TextFile;
import com.github.tomakehurst.wiremock.standalone.MappingsSource;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.github.tomakehurst.wiremock.stubbing.StubMappings;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.github.tomakehurst.wiremock.common.Json.writePrivate;
import static com.google.common.collect.Iterables.filter;

/**
 * Created by rogerionobrega on 20/04/18.
 */
public class SidatMappingSource implements MappingsSource {
  private final FileSource mappingsFileSource;
  private final Map<String, String> fileNameMap;

  public SidatMappingSource(FileSource mappingsFileSource) {
    this.mappingsFileSource = mappingsFileSource;
    fileNameMap = new HashMap<>();
  }

  @Override
  public void save(List<StubMapping> stubMappings) {
    for (StubMapping mapping: stubMappings) {
      if (mapping != null && mapping.isDirty()) {
        save(mapping);
      }
    }
  }

  @Override
  public void save(StubMapping stubMapping) {
    String mappingFileName = fileNameMap.get(stubMapping.getName());
    if (mappingFileName == null) {
      mappingFileName = stubMapping.getName() + ".json";
    }

    mappingsFileSource.writeTextFile(mappingFileName, writePrivate(stubMapping));
    fileNameMap.put(stubMapping.getName(), mappingFileName);
    stubMapping.setDirty(false);
  }

  @Override
  public void remove(StubMapping stubMapping) {
    String mappingFileName = fileNameMap.get(stubMapping.getName());
    mappingsFileSource.deleteFile(mappingFileName);
    fileNameMap.remove(stubMapping.getName());
  }

  @Override
  public void removeAll() {
    for (String filename: fileNameMap.values()) {
      mappingsFileSource.deleteFile(filename);
    }
    fileNameMap.clear();
  }

  @Override
  public void loadMappingsInto(StubMappings stubMappings) {
    if (!mappingsFileSource.exists()) {
      return;
    }
    Iterable<TextFile> mappingFiles = filter(mappingsFileSource.listFilesRecursively(), AbstractFileSource.byFileExtension("json"));
    for (TextFile mappingFile: mappingFiles) {
      StubMapping mapping = StubMapping.buildFrom(mappingFile.readContentsAsString());
      mapping.setDirty(false);
      stubMappings.addMapping(mapping);
      fileNameMap.put(mapping.getName(), mappingFile.getPath());
    }
  }
}
