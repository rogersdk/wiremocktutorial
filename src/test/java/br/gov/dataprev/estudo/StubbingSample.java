package br.gov.dataprev.estudo;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by rogerionobrega on 19/04/18.
 */
public class StubbingSample {

  public static final String URL_HELLO_WORLD = "http://localhost:8080/";

  @Rule
  public WireMockRule wireMockRule = new WireMockRule(
          options()
          .port(8080)
  );

  @Test
  public void basicStubbingTest() throws IOException {
    wireMockRule.stubFor(get(urlEqualTo("/"))
            .willReturn(aResponse()
                    .withHeader("Content-Type", "text/plain")
                    .withBody("Hello world Mocked!")));

    DefaultHttpClient httpClient = new DefaultHttpClient();
    HttpGet getRequest = new HttpGet(
            URL_HELLO_WORLD);


    HttpResponse response = httpClient.execute(getRequest);

    BufferedReader br = new BufferedReader(
            new InputStreamReader((response.getEntity().getContent())));

    String output;
    System.out.println("Output from Server .... \n");
    while ((output = br.readLine()) != null) {
      if (!output.isEmpty()) {
        System.out.println("Resposta = " + output);

        assertNotEquals("Hello world!", response);
      }
    }
  }

}
