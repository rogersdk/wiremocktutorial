package br.gov.dataprev.estudo;

import com.github.tomakehurst.wiremock.common.ProxySettings;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.google.gson.Gson;
import jdk.net.SocketFlow;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthSchemeRegistry;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.json.GsonJsonParser;
import org.springframework.boot.json.JsonParser;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by rogerionobrega on 18/04/18.
 */
public class WireMockProxying {

  private static final String URL_POSTS = "http://localhost:8080/posts";
  private static final String URL_POSTS_LIVE = "http://w3b11.sec.prevnet:18200/libService/rest/lib/responsavelCei?parametro=";

  @Rule
  public WireMockRule wireMockRule = new WireMockRule(
          options()
                  .port(8080)
  );

  @Test
  public void basicProxyTest() throws IOException {
    wireMockRule.stubFor(get(urlEqualTo("/"))
            .willReturn(
                    aResponse().proxiedFrom("http://localhost:8888/")
            )
    );

    DefaultHttpClient httpClient = new DefaultHttpClient();
    HttpGet getRequest = new HttpGet(
            StubbingSample.URL_HELLO_WORLD);

    HttpResponse response = httpClient.execute(getRequest);

    String resposta = EntityUtils.toString(response.getEntity(), "UTF-8");

    assertEquals("Hello World! Server = App 2", resposta);
  }

  @Test
  public void usingRestTemplateTest() {
    wireMockRule.stubFor(get(urlEqualTo("/posts"))
            .willReturn(aResponse().proxiedFrom("http://localhost:8888/")));

    RestTemplate restTemplate = new RestTemplate();
    Post post = restTemplate.getForObject("http://localhost:8080/posts", Post.class);
    assertEquals("App 2", post.getHost());
  }
}
