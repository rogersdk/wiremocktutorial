package br.gov.dataprev.estudo;

import com.github.tomakehurst.wiremock.common.*;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.github.tomakehurst.wiremock.recording.RecordSpec;
import com.github.tomakehurst.wiremock.recording.RecordSpecBuilder;
import com.github.tomakehurst.wiremock.standalone.JsonFileMappingsSource;
import com.github.tomakehurst.wiremock.standalone.MappingsSource;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.github.tomakehurst.wiremock.stubbing.StubMappings;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.common.Json.writePrivate;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static com.google.common.collect.Iterables.filter;
import static org.junit.Assert.assertEquals;

/**
 * Created by rogerionobrega on 20/04/18.
 */
public class WireMockRecordingAndPlaybackJUnitRuleTest {

  public static final String PROXY_BASE_URL = "http://localhost:8888/";

  @Rule
  public WireMockRule wireMockRule = new WireMockRule(
          options()
                  .bindAddress("localhost")
                  .port(8080)
          .mappingSource(new SidatMappingSource(options().filesRoot().child("mappings"))));

  @Test
  public void recordingWithProxyTest() {
    List<StubMapping> stubMappingsList = wireMockRule.getStubMappings();

    stubFor(get(urlMatching("/posts/.*"))
            .willReturn(aResponse().proxiedFrom(PROXY_BASE_URL)));

    RecordSpecBuilder recordSpecBuilder = new RecordSpecBuilder();
    recordSpecBuilder
            .forTarget(PROXY_BASE_URL);

    wireMockRule.startRecording(recordSpecBuilder);

    RestTemplate restTemplate = new RestTemplate();
    Post post = restTemplate.getForObject("http://localhost:8080/posts/1", Post.class);

    List<StubMapping> stubMappingList = wireMockRule.stopRecording().getStubMappings();

    assertEquals(1, post.getPostId().intValue());
    assertEquals("App 2", post.getHost());
  }
}
