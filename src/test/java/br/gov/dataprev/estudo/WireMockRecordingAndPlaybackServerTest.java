package br.gov.dataprev.estudo;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.github.tomakehurst.wiremock.standalone.MappingsSource;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.github.tomakehurst.wiremock.stubbing.StubMappings;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import javax.rmi.CORBA.Stub;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by rogerionobrega on 19/04/18.
 */
public class WireMockRecordingAndPlaybackServerTest {

  @Test
  public void recordAndPlaybackTest() {
    WireMockServer wireMockServer = new WireMockServer(options().port(8080));
    wireMockServer.start();
    wireMockServer.startRecording("http://localhost:8080/");

    wireMockServer.stubFor(get(urlMatching("/posts/.*"))
            .willReturn(aResponse().proxiedFrom("http://localhost:8888/")));

    RestTemplate restTemplate = new RestTemplate();
    Post post = restTemplate.getForObject("http://localhost:8080/posts/12", Post.class);

    assertEquals("App 2", post.getHost());

    List<StubMapping> recordedMappings = wireMockServer.stopRecording().getStubMappings();

    assertNotNull(recordedMappings);
    assertTrue(recordedMappings.size() > 0);
  }

}
