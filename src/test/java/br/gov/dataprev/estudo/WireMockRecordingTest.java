package br.gov.dataprev.estudo;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import org.junit.Rule;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by rogerionobrega on 26/03/18.
 */
public class WireMockRecordingTest {

  private static final Logger LOG = LoggerFactory.getLogger(WireMockRecordingTest.class);

//  @Rule
//  public WireMockRule wireMockRule = new WireMockRule(8090);

  @Test
  public void clientRecordingTest() {
    // Static DSL
    WireMock wireMockClient = new WireMock(8090);
    wireMockClient.startStubRecording(WireMockTest.URL_POSTS);
    List<StubMapping> recordedMappings = wireMockClient.stopStubRecording().getStubMappings();

    LOG.debug("=========================DEBUGING STUBS==============================================");
    for(StubMapping stubs: recordedMappings) {
      LOG.debug(String.format("Stub-> %s", stubs.getResponse().getBody()));
    }
    LOG.debug("=========================END DEBUGING STUBS==========================================");

  }
}
