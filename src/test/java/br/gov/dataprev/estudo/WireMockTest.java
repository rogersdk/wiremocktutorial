package br.gov.dataprev.estudo;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.github.tomakehurst.wiremock.recording.RecordSpecBuilder;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import org.junit.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;


/**
 * Created by rogerionobrega on 22/03/18.
 */
public class WireMockTest {

  public static final String URL = "http://localhost:8080";

  public static final String URL_POSTS = "http://localhost:8080/posts";

  public static final String WIREMOCK_URL = "http://localhost:8089";

  @Rule
  public WireMockRule wireMockRule = new WireMockRule(8089);

  @Before
  public void before() {
    stubFor(get(urlMatching("/"))
            .willReturn(aResponse()
                    .withHeader("Content-Type", "text/plain")
                    .withBody("Hello world Roger!")));

    stubFor(get(urlMatching("/posts")).willReturn(okJson("{\"userId\":2,\"id\":2,\"title\":\"Birl title 2\",\"body\":\"Body Birlder 2\"}")));

    stubFor(get(urlMatching("http://localhost:8080/posts"))
            .willReturn(
                    okJson("{\"userId\":3,\"id\":3,\"title\":\"Birl title 3\",\"body\":\"Body Birlder 3\"}")
                            .proxiedFrom("http://localhost:8089/posts")));
  }

  @Test
  public void springBootRealApiTest() {
//    Post post2 = new RestTemplate().getForObject("http://jsonplaceholder.typicode.com/posts/2", Post.class);
//    Assert.assertEquals(new Integer(2), post2.getId());

    RestTemplate restTemplate = new RestTemplate();

    Post post = restTemplate.getForObject(URL_POSTS, Post.class);
    Assert.assertEquals(new Integer(1), post.getPostId());
  }

  @Test
  public void basicStubbingTest() {
    String response = new RestTemplate().getForObject(WIREMOCK_URL, String.class);
    Assert.assertEquals("Hello world Roger!", response);

    Post post = new RestTemplate().getForObject(WIREMOCK_URL + "/posts", Post.class);
    Assert.assertEquals(new Integer(2), post.getPostId());

    Post post2 = new RestTemplate().getForObject("http://jsonplaceholder.typicode.com/posts/2", Post.class);
    Assert.assertEquals(new Integer(2), post2.getPostId());

  }

//  @Test
  public void proxiedRequestTest() {
    // Low priority catch-all proxies to otherhost.com by default
    stubFor(get(urlMatching(URL + "/.*")).atPriority(10)
        .willReturn(aResponse().proxiedFrom("http://0.0.0.0:8089")));


    // High priority stub will send a Service Unavailable response
    // if the specified URL is requested
    stubFor(get(urlEqualTo(URL)).atPriority(1)
        .willReturn(aResponse().withStatus(503)));
  }

//  @Test
  public void recordingRequestTest() {
    List<StubMapping> mappings = wireMockRule.snapshotRecord().getStubMappings();

    String response = new RestTemplate().getForObject(WIREMOCK_URL + "/posts", String.class);

    Assert.assertEquals("Hello world Roger!", response);
    Assert.assertEquals(0, response);
  }

}
